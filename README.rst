..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit ../src/vmod_blobsha256.vcc and run make instead
..

.. role:: ref(emphasis)

===============
vmod_blobsha256
===============

----------------------------------------------
SHA256 digests and hmacs for the VCL blob type
----------------------------------------------

:Manual section: 3

SYNOPSIS
========

.. parsed-literal::

  import blobsha256 [as name] [from "path"]
  
  new xhmac = blobsha256.hmac(BLOB key)
  
      BLOB xhmac.hmac(BLOB msg)
   
  new xblob = blobsha256.blob(BLOB blob)
  
      BLOB xblob.hash()
   
  BLOB hashf(BLOB msg)
  
  BLOB hmacf(BLOB msg, BLOB key)
  
  STRING version()
  


.. _blobsha256.hmac():

new xhmac = blobsha256.hmac(BLOB key)
-------------------------------------

Description
	Creates an object that generates HMACs based on SHA256 and the
	given ``key``.

Example
	new key = blobcode.blob(BASE64, "a2V5");
	new hmac = blobsha256.hmac(key.get());

.. _xhmac.hmac():

BLOB xhmac.hmac(BLOB msg)
-------------------------

Description
	Returns the HMAC-SHA256 message authentication code for
	``msg`` based on the key provided in the constructor.

Example
	set req.http.hmac = hmac.hmac(blobcode.decode(BASE64, "Zm9v"));

.. _blobsha256.blob():

new xblob = blobsha256.blob(BLOB blob)
--------------------------------------

Description
	Creates an object that returns the SHA256 digest of the given
	``blob``.

Example
	new key = blobcode.blob(BASE64, "Zm9v");
	new foo = blobsha256.blob(key.get());

.. _xblob.hash():

BLOB xblob.hash()
-----------------

Description
	Returns the SHA256 digest for the blob given in the constructor.

Example
	set req.http.X-Hash = blobcode.encode(BASE64, foo.hash());

.. _blobsha256.hashf():

BLOB hashf(BLOB msg)
--------------------

Description
	Returns the SHA256 digest for ``msg``.

.. _blobsha256.hmacf():

BLOB hmacf(BLOB msg, BLOB key)
------------------------------

Description
	Returns the SHA256 HMAC for ``msg`` based on ``key``.

.. _blobsha256.version():

STRING version()
----------------

Description
	Returns the version string for this VMOD.

Example
	std.log("Using VMOD blobsha256 version " + blobsha256.version());

REQUIREMENTS
------------

This version of the VMOD requires at least Varnish 5.1. See the
project repository for versions that are compatible with other Varnish
versions.

COPYRIGHT
=========

::

  Copyright (c) 2016 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 
  See LICENSE
 
