/*-
 * Copyright 2016 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "config.h"

#include "cache/cache.h"
#include "vsha256.h"
#include "vas.h"
#include "vcl.h"
#include "vsb.h"

#include "vcc_if.h"

#define SHA256_BLOCKSZ 64

#define ERR(ctx, msg) \
	errmsg((ctx), "vmod blobsha256 error: " msg)

#define ERRNOMEM(ctx, msg) \
	ERR((ctx), msg ", out of space")

struct vmod_blobsha256_hmac {
	unsigned magic;
#define VMOD_BLOBSHA256_HMAC_MAGIC 0x96026563
	SHA256_CTX inner_ctx;
	SHA256_CTX outer_ctx;
};

struct vmod_blobsha256_blob {
	unsigned magic;
#define VMOD_BLOBSHA256_BLOB_MAGIC 0x2067c219
	struct vrt_blob hash;
};

static void
errmsg(VRT_CTX, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	if (ctx->method == VCL_MET_INIT) {
		AN(ctx->msg);
		VSB_vprintf(ctx->msg, fmt, args);
		VRT_handling(ctx, VCL_RET_FAIL);
	}
	else if (ctx->vsl)
		VSLbv(ctx->vsl, SLT_VCL_Error, fmt, args);
	else
		/* Should this ever happen in vcl_fini() ... */
		VSL(SLT_VCL_Error, 0, fmt, args);
	va_end(args);
}

static inline void
digest(VCL_BLOB restrict const b, uint8_t *restrict digest)
{
	SHA256_CTX ctx[1];

	SHA256_Init(ctx);
	SHA256_Update(ctx, b->blob, b->len);
	SHA256_Final(digest, ctx);
}

static void
hmac_init(VCL_BLOB restrict const key, SHA256_CTX * restrict const inner_ctx,
	  SHA256_CTX * restrict const outer_ctx)
{
	uint8_t k[SHA256_BLOCKSZ] = { 0 }, innerk[SHA256_BLOCKSZ],
		outerk[SHA256_BLOCKSZ];

	if (key->len <= SHA256_BLOCKSZ)
		memcpy(k, key->blob, key->len);
	else
		digest(key, k);

	for (int i = 0; i < SHA256_BLOCKSZ; i++) {
		innerk[i] = k[i] ^ 0x36;
		outerk[i] = k[i] ^ 0x5c;
	}
	SHA256_Init(inner_ctx);
	SHA256_Init(outer_ctx);
	SHA256_Update(inner_ctx, innerk, SHA256_BLOCKSZ);
	SHA256_Update(outer_ctx, outerk, SHA256_BLOCKSZ);
}

static void
hmac_fini(VCL_BLOB restrict const msg, SHA256_CTX * restrict const inner_ctx,
	  SHA256_CTX * restrict const outer_ctx, uint8_t * restrict const hmac)
{
	uint8_t inner_digest[SHA256_LEN];

	/* Hash the message with the inner key */
	SHA256_Update(inner_ctx, msg->blob, msg->len);
	SHA256_Final(inner_digest, inner_ctx);

	/* Hash the result with the outer key */
	SHA256_Update(outer_ctx, inner_digest, SHA256_LEN);
	SHA256_Final(hmac, outer_ctx);
}

/* Object hmac */

VCL_VOID
vmod_hmac__init(VRT_CTX, struct vmod_blobsha256_hmac **hmacp,
		const char *vcl_name, VCL_BLOB key)
{
	struct vmod_blobsha256_hmac *hmac;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(hmacp);
	AZ(*hmacp);
	AN(vcl_name);
	ALLOC_OBJ(hmac, VMOD_BLOBSHA256_HMAC_MAGIC);
	AN(hmac);
	*hmacp = hmac;

	hmac_init(key, &hmac->inner_ctx, &hmac->outer_ctx);
}

VCL_BLOB
vmod_hmac_hmac(VRT_CTX, struct vmod_blobsha256_hmac *h, VCL_BLOB msg)
{
	struct vrt_blob *b;
	uintptr_t snap;
	SHA256_CTX inner_ctx[1], outer_ctx[1];
	uint8_t *hmac;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(h, VMOD_BLOBSHA256_HMAC_MAGIC);

	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	snap = WS_Snapshot(ctx->ws);
	if ((b = WS_Alloc(ctx->ws, sizeof(*b))) == NULL) {
		ERRNOMEM(ctx, "allocating blob in hmac.hmac()");
		return NULL;
	}
	if ((hmac = WS_Alloc(ctx->ws, SHA256_LEN)) == NULL) {
		WS_Reset(ctx->ws, snap);
		ERRNOMEM(ctx, "allocating hash result in hmac.hmac()");
		return NULL;
	}
	b->len = SHA256_LEN;

	memcpy(inner_ctx, &h->inner_ctx, sizeof(SHA256_CTX));
	memcpy(outer_ctx, &h->outer_ctx, sizeof(SHA256_CTX));

	hmac_fini(msg, inner_ctx, outer_ctx, hmac);
	b->blob = hmac;
	return b;
}

VCL_VOID
vmod_hmac__fini(struct vmod_blobsha256_hmac **hmacp)
{
	struct vmod_blobsha256_hmac *hmac;

	AN(*hmacp);
	hmac = *hmacp;
	*hmacp = NULL;
	CHECK_OBJ_NOTNULL(hmac, VMOD_BLOBSHA256_HMAC_MAGIC);
	FREE_OBJ(hmac);
}

/* Object blob */

VCL_VOID
vmod_blob__init(VRT_CTX, struct vmod_blobsha256_blob **blobp,
		const char *vcl_name, VCL_BLOB b)
{
	struct vmod_blobsha256_blob *blob;
	uint8_t *hash;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(blobp);
	AZ(*blobp);
	AN(vcl_name);
	ALLOC_OBJ(blob, VMOD_BLOBSHA256_BLOB_MAGIC);
	AN(blob);
	*blobp = blob;

	hash = malloc(SHA256_LEN);
	if (hash == NULL) {
		ERRNOMEM(ctx, "allocating hash in blob constructor");
		return;
	}

	blob->hash.len = SHA256_LEN;
	digest(b, hash);
	blob->hash.blob = hash;
}

VCL_BLOB
vmod_blob_hash(VRT_CTX, struct vmod_blobsha256_blob *blob)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(blob, VMOD_BLOBSHA256_BLOB_MAGIC);
	return &blob->hash;
}

VCL_VOID
vmod_blob__fini(struct vmod_blobsha256_blob **blobp)
{
	struct vmod_blobsha256_blob *blob;

	AN(*blobp);
	blob = *blobp;
	*blobp = NULL;
	CHECK_OBJ_NOTNULL(blob, VMOD_BLOBSHA256_BLOB_MAGIC);
	FREE_OBJ(blob);
}

/* Functions */

VCL_BLOB
vmod_hashf(VRT_CTX, VCL_BLOB msg)
{
	struct vrt_blob *b;
	uintptr_t snap;
	uint8_t *hash;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	if (msg == NULL)
		return NULL;

	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	snap = WS_Snapshot(ctx->ws);
	if ((b = WS_Alloc(ctx->ws, sizeof(*b))) == NULL) {
		ERRNOMEM(ctx, "allocating blob in hash()");
		return NULL;
	}
	if ((hash = WS_Alloc(ctx->ws, SHA256_LEN)) == NULL) {
		WS_Reset(ctx->ws, snap);
		ERRNOMEM(ctx, "allocating hash result in hash()");
		return NULL;
	}
	b->len = SHA256_LEN;
	digest(msg, hash);
	b->blob = hash;
	return b;
}

VCL_BLOB
vmod_hmacf(VRT_CTX, VCL_BLOB msg, VCL_BLOB key)
{
	struct vrt_blob *b;
	uintptr_t snap;
	SHA256_CTX inner_ctx[1], outer_ctx[1];
	uint8_t *hmac;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	if (msg == NULL || key == NULL)
		return NULL;

	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	snap = WS_Snapshot(ctx->ws);
	if ((b = WS_Alloc(ctx->ws, sizeof(*b))) == NULL) {
		ERRNOMEM(ctx, "allocating blob in blobsha256.hmac()");
		return NULL;
	}
	if ((hmac = WS_Alloc(ctx->ws, SHA256_LEN)) == NULL) {
		WS_Reset(ctx->ws, snap);
		ERRNOMEM(ctx, "allocating hash result in blobsha256.hmac()");
		return NULL;
	}
	b->len = SHA256_LEN;

	hmac_init(key, inner_ctx, outer_ctx);
	hmac_fini(msg, inner_ctx, outer_ctx, hmac);
	b->blob = hmac;
	return b;
}

VCL_STRING
vmod_version(VRT_CTX __attribute__((unused)))
{
	return VERSION;
}
